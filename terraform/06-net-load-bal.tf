# создание и настройка nlb (network load balancer)
resource "yandex_lb_network_load_balancer" "nlb" {
  name = "nlb"

  listener {
    name = "listener-web-servers"
    port = 80
    external_address_spec {
      ip_version = "ipv4"
    }
  }

  attached_target_group {
    target_group_id = yandex_lb_target_group.web-servers.id

    healthcheck {
      name = "http"
      unhealthy_threshold = 3
      healthy_threshold   = 3
      http_options {
        port = 80
        path = "/"
      }
    }
  }
}
# Перечисление целей для группы распределения нагрузки
resource "yandex_lb_target_group" "web-servers" {
  name = "web-servers-target-group"

  target {
    subnet_id = yandex_vpc_subnet.subnet.id
    address   = module.instance_2.internal_ip_address_vm
  }
}