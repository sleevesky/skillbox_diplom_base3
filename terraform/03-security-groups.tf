resource "yandex_vpc_security_group" "sec_group" {
  name       = "cluster-vm-sg"
  network_id = yandex_vpc_network.network.id

  ingress {
    description    = "Allow any ingress traffic"
    protocol       = "ANY"
    v4_cidr_blocks = ["0.0.0.0/0"]
    from_port      = 0
    to_port        = 65535
  }
  #ingress {
  #  protocol    = "TCP"
  #  description = "balancer"
  # 
  # v4_cidr_blocks = ["0.0.0.0/0"]
  # port           = 8080
  #}

  #ingress {
  #  protocol       = "TCP"
  #  description    = "ssh"
  #  v4_cidr_blocks = ["0.0.0.0/0"]
  #  port           = 22
  #}
  #ingress {
  #  protocol       = "TCP"
  #  description    = "ext-https"
  #  v4_cidr_blocks = ["0.0.0.0/0"]
  #  port           = 443
  #}

  egress {
    description    = "Allow any egress traffic"
    protocol       = "ANY"
    v4_cidr_blocks = ["0.0.0.0/0"]
    from_port      = 0
    to_port        = 65535
  }
}