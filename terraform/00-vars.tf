variable "zone" {
  type = string
}

variable "yandex_token" {
  type = string
}

variable "yandex_cloud_id" {
  type = string
}

variable "yandex_folder_id" {
  type = string
}

variable "name_dns_record" {
  type = string
}

variable "CI_PROJECT_DIR" {
  type = string
}
# chose images - yc compute image list --folder-id standard-images
variable "family_images" {
  type = string
}
variable "user_name" {
  type = string
}
variable "ssh_authorized_keys" {
  type = string
}
variable "SSH_privat" {
  type = string
}