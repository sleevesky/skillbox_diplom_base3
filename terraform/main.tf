terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }
  required_version = ">= 0.13"
  backend "http" {
  }
}
# по шаблону создаем instance
 module "instance_1" {
  source = "./instance"
  name   = "monitor"
  VPC_subnet_idi = yandex_vpc_subnet.subnet.id
  inst_security_group_ids = ["${yandex_vpc_security_group.sec_group.id}"]
  inst_family_images = "${var.family_images}"
  zone_inst = "${var.zone}"
  inst_ssh_authorized_keys = var.ssh_authorized_keys
  inst_user_name = var.user_name
  SSH_privat = "${var.SSH_privat}"
}

module "instance_2" {
  source = "./instance"
  name   = "web1"
  VPC_subnet_idi = yandex_vpc_subnet.subnet.id
  inst_security_group_ids = ["${yandex_vpc_security_group.sec_group.id}"]
  inst_family_images = "${var.family_images}"
  zone_inst = "${var.zone}"
  inst_ssh_authorized_keys = var.ssh_authorized_keys
  inst_user_name = var.user_name
  SSH_privat = "${var.SSH_privat}"
}
module "instance_3" {
  source = "./instance"
  name   = "testweb"
  VPC_subnet_idi = yandex_vpc_subnet.subnet.id
  inst_security_group_ids = ["${yandex_vpc_security_group.sec_group.id}"]
  inst_family_images = "${var.family_images}"
  zone_inst = "${var.zone}"
  inst_ssh_authorized_keys = var.ssh_authorized_keys
  inst_user_name = var.user_name
  SSH_privat = "${var.SSH_privat}"
}

# Generate inventory file
resource "local_file" "inventory" {
  filename = "${var.CI_PROJECT_DIR}/ansible/hosts"
  content = <<EOF
[webserver]
  web1      ansible_host=${module.instance_2.external_ip_address_vm}
  testweb   ansible_host=${module.instance_3.external_ip_address_vm}
  monitor   ansible_host=${module.instance_1.external_ip_address_vm}
  
[monitor]
  monitor   ansible_host=${module.instance_1.external_ip_address_vm}
 EOF
 
depends_on = [
    module.instance_3
  ]
}