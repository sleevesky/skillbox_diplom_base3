resource "yandex_vpc_network" "network" {
  name        = "network"
  description = "Network"

}

resource "yandex_vpc_subnet" "subnet" {
  name           = "subnet"
  description    = "Subnet"
  zone           = "${var.zone}"
  network_id     = yandex_vpc_network.network.id
  v4_cidr_blocks = ["10.0.0.0/24"]
}

