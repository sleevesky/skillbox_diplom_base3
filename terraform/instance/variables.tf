variable "zone_inst" {
  type = string
}

variable "instance_root_disk" {
  default = "10"
}

# chose images - yc compute image list --folder-id standard-images
variable "inst_family_images" {
  type = string
}

variable "inst_user_name" {
  type = string
}

variable "inst_ssh_authorized_keys" {
  type = string
}
variable "name" {
  description = "Instance name"
  type        = string
}
variable "VPC_subnet_idi" {
  description = "VPC subnet network id"
  type        = string
}
variable "inst_security_group_ids" {
  description = "security group for instance"
  type        = list
}
variable "SSH_privat" {
  type = string
}