terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
      version = "0.81.0"
    }
  }
}

data "yandex_compute_image" "family_image" {
  family = "${var.inst_family_images}"
}

resource "yandex_compute_instance" "vm-test1" {
  name                      = "${var.name}"
  description               = "skillbox_project"
  allow_stopping_for_update = true

  resources {
    cores         = 2
    memory        = 2
  }

  boot_disk {
    auto_delete = true
    
    initialize_params {
      image_id    = "${data.yandex_compute_image.family_image.id}"
    # name        = "disk-root"
      description = "Disk for the root"
      size        = "${var.instance_root_disk}"
    }
  }

  network_interface {
    subnet_id = "${var.VPC_subnet_idi}"
    nat       = true
    #правила доступа настраивается в файле 04-security-groups
    security_group_ids = "${var.inst_security_group_ids}"
  }

  zone = var.zone_inst
  metadata = {
    user-data = templatefile("${path.module}/meta.tftpl.yml", {user_name = "${var.inst_user_name}", ssh_authorized_keys = "${var.inst_ssh_authorized_keys}"})
    
  }
  provisioner "remote-exec" {
    inline = ["sudo apt update", "echo check connection done"]

    connection {
      host        = "${yandex_compute_instance.vm-test1.network_interface.0.nat_ip_address}"
      type        = "ssh"
      user        = "${var.inst_user_name}"
      private_key = var.SSH_privat
    }
  }

  
}

