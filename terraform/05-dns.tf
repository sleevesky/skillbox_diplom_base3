resource "yandex_dns_zone" "dns_zone" {
  name        = "my-public-zone"
  description = "skbx"

  labels = {
    label1 = "test-public"
  }

  zone    = "${var.name_dns_record}"
  public  = true
}
resource "yandex_dns_recordset" "rs1" {
  zone_id = yandex_dns_zone.dns_zone.id
  name    = "${var.name_dns_record}"
  type    = "A"
  ttl     = 200
  data    = [(yandex_lb_network_load_balancer.nlb.listener[*].external_address_spec[*].address)[0][0]]
}
resource "yandex_dns_recordset" "rs2" {
  zone_id = yandex_dns_zone.dns_zone.id
  name    = "prod.${var.name_dns_record}"
  type    = "A"
  ttl     = 200
  data    = [module.instance_2.external_ip_address_vm]
}

resource "yandex_dns_recordset" "rs3" {
  zone_id = yandex_dns_zone.dns_zone.id
  name    = "dev.${var.name_dns_record}"
  type    = "A"
  ttl     = 200
  data    = [module.instance_3.external_ip_address_vm]
}

resource "yandex_dns_recordset" "rs4" {
  zone_id = yandex_dns_zone.dns_zone.id
  name    = "monitor.${var.name_dns_record}"
  type    = "A"
  ttl     = 200
  data    = [module.instance_1.external_ip_address_vm]
}

resource "yandex_dns_recordset" "rs5" {
  zone_id = yandex_dns_zone.dns_zone.id
  name    = "grafana.${var.name_dns_record}"
  type    = "A"
  ttl     = 200
  data    = [module.instance_1.external_ip_address_vm]
}