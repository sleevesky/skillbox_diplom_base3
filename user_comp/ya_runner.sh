#!/bin/bash
yc compute instance create \
  --name runner \
  --zone ru-central1-a \
  --network-interface subnet-name=default-ru-central1-a,nat-ip-version=ipv4 \
  --create-boot-disk image-folder-id=standard-images,size=10,image-family=ubuntu-2204-lts \
  --ssh-key /home/u18/.ssh/id_rsa.pub

sleep 50

PUBLIC_IP=$(yc compute instance get --format=json --name=runner | jq -r .network_interfaces[0].primary_v4_address.one_to_one_nat.address) && cat <<EOF > hosts
  [runner]
  run1 ansible_host=$PUBLIC_IP 
EOF
read -p "enter your token runner gitlab ci : " reg_token
export ANSIBLE_HOST_KEY_CHECKING=False
ansible-playbook --private-key /home/u18/.ssh/id_rsa --user yc-user -i hosts -e "reg_token="${reg_token}"" install_runner.yml -vvv
