![alt text](sb_dip2.png "shcema")

> Для работы с проектом Вам потребуется:
#### клонировать к себе репозиторий и из папки user_comp запустить скрипт - он в облаке яндекс создаст instance - агента gitlab-runner
#### предварительно нужно настроить Yandex Cli чтоб скрипт смог к облаку подключиться, и с гитлаба скопировать в буфер токен для подключения агента gitlab-runnerранера из раздела settings - CI/CD - Runner
#### выключить shared runners: в разделе settings - CI/CD - Runner - выключаем переключатель "Enable shared runners for this project" 
#### для удаления виртуальной машины с runner'ом используем команду с локального компьютера где настроен и был запущен скрипт, имя виртуальной машины: runner
~~~
yc compute instance delete runner 
~~~

# На компьютере "Client" и сам пользователь должны иметь:


#### Регистрацию в yandex clouds - https://cloud.yandex.ru/docs/getting-started/quickstart-individuals
#### Регистрацию на сайте Gitlab.com - [https://gitlab.com/users/sign\_in](https://gitlab.com/users/sign_in)
> [создаем личный токен](#create-token-gitlab)
#### Регистрацию доменного имени – [reg.ru ](reg.ru)
> [настройка](#setting-site-reg) для yandex cloud
#### Установить Yandex cli - https://cloud.yandex.ru/docs/cli/quickstart 
> [используем для получения значений, что бы добавить в переменные](#variables-gitlab-ci)
#### Установить Git - https://git-scm.com/book/en/v2/Getting-Started-Installing-Git
> [для debug чтоб пушить и клонировать](#setting-git)

### Для отладки нужно:

#### Установить ansible - https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html
> [(для быстрой отладки playbook'ов)](#setting-debug-ansibleplaybook)
#### Установить Docker - https://docs.docker.com/engine/install/ubuntu/
> [(для получения токена в файле config.json который используется для входа в репозиторий gitlab container registry)](#setting-docker-file-config)

#### Далее проводим настройку:

# create token gitlab
[На сайте gitlab.com создаем личный токен](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html) (я поставил галочки api, read\_repository и write\_repositor)

# setting docker file config для deploy  через юнит файл (не обезательно)

Имея персональный личный токен (с предыдущего шага) генерируем токен аутентификации в файле config.json для доступа в [gitlab Container Registry](https://docs.gitlab.com/ee/user/packages/container_registry/authenticate_with_container_registry.html) с помощью команды:

~~~
docker login registry.example.com -u \<username\> -p \<token\>
~~~
# variables gitlab ci
##### Заполняем переменные в проекте – settings – ci/cd – Variables –

Имена переменных оставить идентичные, если не хотите бегать по коду и вносить изменения.

<details>
<summary>Примеры переменных:</summary>

| имя переменной | значение переменной |команда|
| --- | --- | --- |
| config\_docker | c2xlZXZlc2t5OmdscGF0L????S29DTlBzUHhWUUZWWHppQjVM | Токен с файла config.json для раздела pipline unitdock
| family\_images | ubuntu-2204-lts | yc compute image list --folder-id standard-images |
| name\_dns\_record | nedostupnoe.space. | Домен который вы зарегистрировали на reg.ru (Точка в конце обязательна) https://cloud.yandex.ru/docs/dns/operations/resource-record-create (в разделе zone)  |
| repo | gitlab.com/sleevesky/sb\_go\_8081.git | Адресс репозитория с приложением GO |
| SSH\_PRIVATE\_KEY | -----BEGIN OPENSSH PRIVATE KEY-----SitJv3qtGiR9izirkTwsAAAADXUxOEBzbGVldmVza3k=-----END OPENSSH PRIVATE KEY----- | Ключи сгенерированные, вставляем полностью с --- BEGIN по KEY----- |
| SSH\_pubkey | ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIOsd+NWgEzO3/ljLx1LLzKRwSitJv3qtGiR9izirkTws | Публичный ключ (\*.pub) |
| user\_system\_instans | skill | Имя пользователя в операционной системе instansa в облаке |
| ya\_cloud\_id | b1gp4opil7ptctl7qb0d | yc config list |
| ya\_folder\_id | b1gf5rs38b075elqgpcu | yc config list |
| ya\_token | y0\_AgAA?????????AADMhd1uC0MRyljBQkqiW0g06a14KTYS2Ao | yc config list |
| ya\_zone | ru-central1-a | Зона по умолчанию, прописываем ее |
| api_4_clone | glpat-aRQ&&&&r_CzL39gATFG&&f&U | create token gitlab |
| id_cont_registry |4394039| для откатывания на предыдущую версию контейнера, но id этот буден виден в адресной строке gitlab после того как в реджестре будет хоть один контейнер|
| job_arti1| пусто| для того чтоб артифакты получать с прошлых pipline из job где они создались и храняться, эта переменная это номер джобы с артифактами|
|password_git и username_git | | забиваете логин и пароль от репы приватной, если нужно брать для сборки приложения|
</details>


# setting site reg 
На сайте, где зарегистрировали доменное имя в настройках  DNS-серверы и управление зоной – прописываем свой список DNS-серверов ns1.yandexcloud.net, ns2.yandexcloud.net
~~~
https://cloud.yandex.ru/docs/dns/quickstart 
~~~ 
в самом конце (может важно, но нужно точки поставить в конце ns1.yandexcloud.net.)


# setting git

после установки гита настроим его на локальном компьбетре: 
~~~
git config --globaluser.name "YourName"
~~~

~~~
git config --global user.email "youremail@domain.com"
~~~

Мы можем просмотреть все настроенные пункты конфигурации, введя следующую команду: 
~~~
git config --list
~~~

# debug

Делаем клонирование к себе в папку
~~~
https://gitlab.com/sleevesky/sb_dim_z2.git
~~~

После внесения изменений выполняем команду 
~~~
git add . ; git commit -m "comment"; git push
~~~

#### create file hosts for work local playbook
 в папке, где лежит playbook для отладки, нужно создать файл **hosts** с содержимым - ip нужно узнать в вэб интерфейсе yandex cloud или в логах работы ci/cd после джобы deploy

<details>
<summary>мой пример</summary>

# hosts

~~~
[webserver]

web1 ansible_host=158.160.53.28 # ip instans 

~~~

</details>

# setting debug ansibleplaybook

Для проверки ansible playbook'ов переходим в папку с файлом playbook и в консоли используем такую команду

ansible-playbook --private-key **/путь до**  **ssh**  **ключей/имя приватного ключа** --user **имя пользователя указанного в переменных** -i **[hosts](#hosts)** -e "user\_system\_instans= **имя пользователя на указанного в переменных**" skilbox-diploma\_unit.yml -vvv
###### мой пример команды:
~~~
ansible-playbook --private-key /home/u18/skillbox/ssh_ya_sb_dip/ya --user skill -i hosts -e "user_system_instans=skill" skilbox-diploma_unit.yml -vvv
~~~

# connect with ssh key
Для подключения к instans'ам в Yandexcloud используем такую команду:

ssh -i **/путь до**  **ssh**  **ключей/имя приватного ключа** /  имя пользователя (указанного в переменных) @ ipinstans смотрим в вэб интерфейсе Yandexcloud

пример моей команды:
~~~
ssh -i /home/u18/skillbox/ssh_ya_sb_dip/ya skill@158.160.53.28
~~~

# Описание проекта:

По заданным переменным с помощью terraforma создается инфраструктура с балансировщиком, и одним виртуальным компьютером, на котором размещается контейнер с приложением app (написанном на языке GO)

<details>
<summary>По этапам ci/cd</summary>


| имя джобы | назначение джобы|
| --- | --- |
| stages:- validate | Проверка синтаксиса terraform |
| - build | terraform init |
| - deploy | terraform plan |
| - docker\_c | на созданную виртуальную машину устанавливается среда для работы контейнера |
| - create\_app | сборка приложения GO |
| - create\_cont | Упаковка приложения в контейнер с сохранением в gitlab container registry готового контейнера методом Kaniko |
| - deploy\_app | Доставка и развертывание контейнера из репозитория gitlab container registry с пробросом портов 80:8081 - Включен автономный режим, чтобы оставить контейнер работающим в фоновом режиме. |
| - unitdock | Этот метод стоит в ручном запуске.  Второй метод доставки контейнера и запуск через файл unitdock.service |
| - destroy | Удаление всего проекта из Yandexcloud |

</details>

# Этим pipline достигнута цель:

1. развертывание инфраструктуры,
2. сборка и упаковка в контейнер и сохранение контейнера в хранилище gitlabcontainerregistry,
3. администрирование файлом состояния инфраструктуры terraform (plan.cache)
4. доставка и запуск приложения в контейнере на удаленной виртуальной машине.

# Результат работы успешного выполнения pipline можно по адресу
 > http://доменно_имя.??

##### мой пример адреса

~~~
http://nedostupnoe.space
~~~
